var jData;
var tr =[];
$.getJSON( "https://api.myjson.com/bins/14qdrz", function( json ) {
	// localStorage.jData = JSON.stringify(json); 
}).done(function(json){
	jData = json;
	jsonData();
});



//jData = JSON.parse(localStorage.jData);
// console.dir(jData);

function lastid(){
	// var lastKey = parseInt(Object.keys(jData).sort().reverse()[0]);
	var lastKey = Object.keys(jData);
	var k = [];
	for(key in lastKey){
		//console.info(key);
		k.push(parseInt(jData[key].id));
	}
	lkey = Math.max.apply(null, k) + 1;
	return lkey;
}

function filterfunc(filterby,orderby){
	var filterby = filterby || 'salary';
	var orderby = orderby || 'asc';
	//alert(filterby);
	switch(filterby){
		case 'id':
		jData.sort(function(a, b) {
		    return parseFloat(a.id) - parseFloat(b.id);
		});
		break;
		case 'salary':
		jData.sort(function (a, b) {
			return a.salary.localeCompare(b.salary);
		});
		break;

		case 'points':
		jData.sort(function (a, b) {
		    return parseFloat(a.points) - parseFloat(b.points);			
		});
		break;

		case 'rebounds':
		jData.sort(function (a, b) {
		    return parseFloat(a.rebounds) - parseFloat(b.rebounds);			
		});
		break;

		case 'assists':
		jData.sort(function (a, b) {
		    return parseFloat(a.assists) - parseFloat(b.assists);	
		});
		break;

		case 'steals':
		jData.sort(function (a, b) {
		    return parseFloat(a.steals) - parseFloat(b.steals);	
		});
		break;

		case 'blocks':
		jData.sort(function (a, b) {
		    return parseFloat(a.blocks) - parseFloat(b.blocks);				
		});
		break;
	}

	if(orderby==='desc'){
		jData.reverse();
	}

	console.dir(jData);
	jsonData();
}



function jsonData(s,e){
	//console.dir(jData);
	var s = s || $("#min_range").val();
	var e = e || $("#max_range").val();

	/*console.error(s);
	console.warn(e);*/


	/*$.getJSON( "https://api.myjson.clocalStorage.person = JSON.stringify(person); om/bins/14qdrz", function( json ) {*/
		var tr =[];
		$.each( jData, function( key, val ) {

			var salary = Number(val.salary.replace(/[^0-9\.]+/g,""));
				//console.warn('salary-'+salary+'/s-'+s+'/e-'+e);
			

			if(s !== false && e !== false){
				if(salary > s && salary < e){
				// console.info('two');

					

			//console.info(key);
			tr.push('<tr data-key='+key+'><td><a class="edit" href="#" data-id="'+val.id+'">'+val.id+'</a></td><td>'+val.fname+'</td><td>'+val.lname+'</td><td>'+val.salary+'</td><td>'+val.points+'</td><td>'+val.rebounds+'</td><td>'+val.assists+'</td><td>'+val.steals+'</td><td>'+val.blocks+'</td><td><a class="delete" href="#" data-id="'+val.id+'" data-key='+key+'>Delete</td></tr>');

		}


	}
	else{
				//console.info('three');

			tr.push('<tr data-key='+key+'><td><a class="edit" href="#" data-id="'+val.id+'">'+val.id+'</a></td><td>'+val.fname+'</td><td>'+val.lname+'</td><td>'+val.salary+'</td><td>'+val.points+'</td><td>'+val.rebounds+'</td><td>'+val.assists+'</td><td>'+val.steals+'</td><td>'+val.blocks+'</td><td><a class="delete" href="#" data-id="'+val.id+'" data-key='+key+'>Delete</td></tr>');
		}

});
		$('tbody').html(tr.join(''));

	//});
}

$(document).ready(function(){
	$("#min_range").val(1000000);
	$("#max_range").val(10000000);
	jsonData();	

	$('.table').on('click','.delete',function(){
		if(confirm("Do you want to delete this data?")){
			//alert($(this).attr('data-id'));
			var key = parseInt($(this).attr('data-key'));
			//alert(key);
			// jData2 = jData.splice(key,1);
			jData.splice(key,1)
			jsonData();

			//console.dir(jData);
			//$(this).parent().parent().remove();
		}
	});

	$('form[name="addfrm"]').submit(function(e){
		e.preventDefault();
		var newData = {};
		newData["id"] = lastid();
		$(this).find('input[type="text"],input[type="number"]').each(function(){
			var newKey = $(this).attr('name');
			var newVal = $(this).val();
			newData[newKey] = newVal;
			//console.log($(this).attr('name'));
			///console.log($(this).val());
		});
		 //console.dir(newData);
		jData.push(newData);
		//console.dir(jData);
		$('#myModal').modal('hide');
		jsonData();
		
	});

	$(".table").on('click','.edit',function(){
		$('#editModal').modal();
		var id_val = parseInt($(this).attr('data-id'));
		var eid = parseInt($(this).parent().parent().attr('data-key'));
		$('form[name="editfrm"]').attr('data-eid',eid);
		//alert(eid);
		$('form[name="editfrm"]').find('input[name="id"]').val(id_val);
		$('form[name="editfrm"]').find('.form-control').each(function(){
			var fieldName = $(this).attr('name');
			var evalue = jData[eid][fieldName];
			/*console.info(fieldName);
			console.log(jData[eid]);
			console.warn(evalue);*/
			$(this).val(evalue);
		});
	});


	$('form[name="editfrm"]').submit(function(e){
		e.preventDefault();
		var editData = {};
		
		$(this).find('input[type="text"],input[type="number"],input[type="hidden"]').each(function(){
			var editKey = $(this).attr('name');
			var editVal = $(this).val();
			editData[editKey] = editVal;
			/*console.log($(this).attr('name'));
			console.log($(this).val());*/
		});
		//console.dir(editData);
		var eid = $(this).attr('data-eid');
		jData[eid] = editData;
		$('#editModal').modal('hide');
		jsonData();
		// console.dir(newData);
	});

	$('select[name="filterby"],select[name="orderby"]').change(function(){
		var filterby = $('select[name="filterby"]').val().toLowerCase();
		var orderby = $('select[name="orderby"]').val();
		//alert(filter);
		filterfunc(filterby,orderby);
	});

	$( function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 1000000,
			max: 10000000,
			values: [ 1000000, 10000000 ],
			slide: function( event, ui ) {
				jsonData(ui.values[ 0 ],ui.values[ 1 ]);
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
				$("#min_range").val(ui.values[ 0 ]);
				$("#max_range").val(ui.values[ 1 ]);
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	} );

	$('.btn-group .btn').click(function(){
		$('.btn-group .btn').removeClass('active');
		
		var btnVal = $(this).attr('value');
		if(btnVal==='off'){
			$('.filterGroup').find('select').attr('disabled','disabled');
			$(this).addClass('active');
			filterfunc('id');
		} else{
			$('.filterGroup').find('select').removeAttr('disabled','disabled');
			$(this).addClass('active');
		}
	});
	
});
